#!/usr/bin/env bash

COMPOSER=$(which composer)
YARN=$(which yarn)
NPM=$(which npm)

function before_public {
    if [[ $RUN_B == 0 ]];then
        return 0
    fi
    labelText "Generate index.php file"
    cp "app/public/app_${ENVIRONMENT}.php" web/index.php

    labelText "Dump composer autoload"
    $COMPOSER dump-autoload
}

function public_prod {
    if [[ $RUN_B == 0 ]];then
        return 0
    fi
    successText "Run composer in PRODUCTION"
    $COMPOSER install -o -a -n --no-dev --prefer-dist
    writeErrorMessage "Could not run composer. Aborting"

    #labelText "Update .htaccess (prod)"
    #cp app/public/.htaccess.prod web/.htaccess

    return 0
}

function public_dev {
    if [[ $RUN_B == 0 ]];then
        return 0
    fi
    labelText "Run composer in DEVELOPMENT"
    $COMPOSER install -n --prefer-dist
    writeErrorMessage "Could not run composer. Aborting"

    #labelText "Update .htaccess (dev)"
    #cp app/ public/.htaccess.dev  public/.htaccess

    return 0
}

function post_public {
    if [[ $RUN_B == 0 ]];then
        return 0
    fi

    labelText "Remove cache files"
    find var/cache/* -maxdepth 0 -type d | xargs rm -rf

    labelText "Assets install"
    bin/console --env=$ENVIRONMENT assets:install web
}

function frontend_build {
    if [[ $RUN_F == 0 ]];then
        return 0
    fi

    labelText "NPM install"
    $YARN install

    labelText "NPM build"
    $NPM run build

    return 0
}
