'use strict';

let sourceDir = 'gulp/assets/';

module.exports = {
  publicDir: 'web/',
  sourceDir: sourceDir,
  sourceJsDir: sourceDir + 'js/'
};
