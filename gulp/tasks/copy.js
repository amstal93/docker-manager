'use strict';

let gulp = require('gulp');

let config = require('../config');

gulp.task('copy-fonts', function(){
  gulp.src([
    'node_modules/bootstrap/fonts/*.*',
    'node_modules/font-awesome/fonts/*.*'
  ], [{ base: './' }])
    .pipe(gulp.dest(config.publicDir + 'fonts'))
  ;
});

let GR = require('kisphp-gulp-commander');

GR.addTask('copy-fonts');
