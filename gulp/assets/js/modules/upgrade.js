
let Upgrade = new function(){
  let self = this;

  self.spinner = '<div class="text-center"><i class="fa fa-spinner fa-spin fa-2x upgrade-spinner"></i></div>';

  this.run = function () {
    self.runGit();
  };

  self.makeRequest = (url, callbackFunction) => {
    $('#upgrade-response').append(self.spinner);
    $.ajax({
      url: url,
      type: 'post'
    }).done(function(response){
      $('#upgrade-response').append(response);
      $('.upgrade-spinner').remove();
      if (typeof callbackFunction !== 'undefined') {
        self[callbackFunction]();
      }
    });
  };

  self.runGit = () => {
    $('#upgrade-response').append(self.spinner);
    self.makeRequest('/upgrade/git', 'runComposer')
  };

  self.runComposer = () => {
    self.makeRequest('/upgrade/composer', 'runGulp')
  };

  self.runGulp = () => {
    self.makeRequest('/upgrade/gulp', 'upgradeDone')
  };

  self.upgradeDone = () => {
    $('.upgrade-spinner').remove();
    $('#upgrade-response').append('<div class="alert alert-success">Upgrade finished. Please reload the page</div>');
  };

  self.skip = () => {
    $.ajax({
      url: '/upgrade/skip',
      type: 'post'
    }).done(function(){
      $('#upgrade-alert').fadeOut('slow');
    });
  };
};

module.exports = {
  init: () => {
    $('#upgrade').click(function(e){
      e.preventDefault();

      $('#upgrade-response').html('');

      $('#myModal').modal('show');

      Upgrade.run();
    });

    $('#upgrade-skip').click(function(e){
      e.preventDefault();

      Upgrade.skip();
    });
  }
};
