<?php

namespace DockerBundle\Controller;

use DockerBundle\Services\DockerFactory;
use Symfony\Component\HttpFoundation\JsonResponse;

class DeleteController
{
    /**
     * @param string $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function imageAction($id)
    {
        $docker = DockerFactory::createDocker();
        $docker->removeDockerImage($id);

        return new JsonResponse([
            'type' => 'image',
            'id' => $id,
        ]);
    }

    /**
     * @param string $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function processAction($id)
    {
        $docker = DockerFactory::createDocker();
        $docker->removeProcess($id);

        return new JsonResponse([
            'type' => 'process',
            'id' => $id,
        ]);
    }

    /**
     * @param string $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function volumeAction($id)
    {
        $docker = DockerFactory::createDocker();
        $exitCode = $docker->removeVolume($id);

        return new JsonResponse([
            'type' => 'volume',
            'id' => $id,
            'code' => $exitCode,
        ]);
    }

    /**
     * @param string $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function networkAction($id)
    {
        $docker = DockerFactory::createDocker();
        $exitCode = $docker->removeNetwork($id);

        return new JsonResponse([
            'type' => 'network',
            'id' => $id,
            'code' => $exitCode,
        ]);
    }
}
