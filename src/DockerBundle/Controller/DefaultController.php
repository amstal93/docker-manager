<?php

namespace DockerBundle\Controller;

use DockerBundle\Services\DockerFactory;
use DockerBundle\Services\ResponseTable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Template()
 */
class DefaultController extends Controller
{
    /**
     * @return array
     */
    public function indexAction()
    {
        $runner = DockerFactory::createRunner();
        $processes = $runner->getProcesses();

        $parser = DockerFactory::createParser();

        return [
            'processes' => $parser->parseProcessResponse($processes),
        ];
    }

    /**
     * @return array
     */
    public function statsAction()
    {
        $content = DockerFactory::createDocker()->getStats();

        return [
            'stats' => $content,
        ];
    }

    /**
     * @return array
     */
    public function networksAction()
    {
        $content = DockerFactory::createDocker()->getNetworks();

        return [
            'networks' => $content,
        ];
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getStats()
    {
        $content = DockerFactory::createDocker()->getStats();

        return new Response(ResponseTable::drawStats($content));
    }

    /**
     * @return array
     */
    public function volumesAction()
    {
        return [
            'docker' => DockerFactory::createDocker(),
        ];
    }

    /**
     * @return array
     */
    public function imagesAction()
    {
        $images = DockerFactory::createDocker()->getImages();

        return [
            'images' => $images,
        ];
    }

    /**
     * @return array
     */
    public function clusterAction()
    {
        return [
            'docker' => DockerFactory::createDocker(),
        ];
    }
}
