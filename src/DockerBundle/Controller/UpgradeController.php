<?php

namespace DockerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UpgradeController extends Controller
{
    /**
     * @param string $type
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function upgradeAction($type)
    {
        $upd = $this->get('app.updater');

        $buffer = '';
        if ($type === 'git') {
            $buffer .= $upd->runGit();
        }
        if ($type === 'composer') {
            $buffer .= $upd->runComposer();
        }
        if ($type === 'gulp') {
            $buffer .= $upd->runGulp();
        }

        return new Response(nl2br($buffer));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function upgradeSkipAction()
    {
        $this->get('app.updater')->retouchUpgradeLockFile();

        return new JsonResponse([
            'core' => 200,
        ]);
    }
}
