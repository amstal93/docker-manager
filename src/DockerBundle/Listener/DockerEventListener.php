<?php

namespace DockerBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;

class DockerEventListener
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onKernelRequest()
    {
        $needUpgrade = $this->container
            ->get('app.updater')
            ->isUpgradeNeeded()
        ;

        $this->container
            ->get('twig')
            ->addGlobal('needsUpgrade', $needUpgrade)
        ;
    }
}
