<?php

namespace DockerBundle\Services;

use Symfony\Component\Process\Process;

class Runner
{
    /**
     * @var \DockerBundle\Services\GuzzleCommander
     */
    private $commander;

    /**
     * @param \DockerBundle\Services\GuzzleCommander $commander
     */
    public function __construct(GuzzleCommander $commander)
    {
        $this->commander = $commander;
    }

    /**
     * @return string
     */
    public function getImages()
    {
        $process = new Process('docker images --no-trunc --format ' . implode(Docker::DELIMITER, Formatter::getImagesFormat()));
        $process->run();

        return $process->getOutput();
    }

    /**
     * @return string
     */
    public function getProcesses()
    {
        $process = new Process('docker ps -a --no-trunc --format ' . implode(Docker::DELIMITER, Formatter::getProcessesFormat()));
        $process->run();

        return $process->getOutput();
    }

    /**
     * @return string
     */
    public function getNetworks()
    {
        $command = 'docker network ls --no-trunc --format ' . implode(Docker::DELIMITER, Formatter::getNetworksFormat());
        $process = new Process($command);
        $process->run();

        return $process->getOutput();
    }

    /**
     * @return string
     */
    public function getVolumes()
    {
        $process = new Process('docker volume ls ');
        $process->run();

        return $process->getOutput();
    }

    /**
     * @return string
     */
    public function getStats()
    {
        $process = new Process('docker stats --no-stream --format ' . implode(Docker::DELIMITER, Formatter::getStatsFormat()));
        $process->run();

        return $process->getOutput();
    }

    /**
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getClusterHealth()
    {
        return $this->commander->getClusterHealth();
    }

    /**
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getIndices()
    {
        return $this->commander->getIndices();
    }

    /**
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getInfo()
    {
        return $this->commander->getInfo();
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function removeProcess($id)
    {
        $volumeId = $this->getVolumeIdForProcessId($id);

        $process = new Process('docker rm -f ' . $id);
        $process->run();

        $processOutput = $process->getOutput();

        if (!empty($volumeId)) {
            $this->removeVolume($volumeId);
        }

        return $processOutput;
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function removeVolume($id)
    {
        $process = new Process('docker volume rm ' . $id);
        $process->run();

        return $process->getExitCode();
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function removeDockerImage($id)
    {
        $process = new Process('docker rmi -f ' . $id);
        $process->run();

        return $process->getOutput();
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function removeDockerNetwork($id)
    {
        $process = new Process('docker network rm ' . $id);
        $process->run();

        return $process->getOutput();
    }

    /**
     * @return string
     */
    public static function getLatestLocalHash()
    {
        $process = new Process('git rev-parse --verify HEAD');
        $process->run();

        return $process->getOutput();
    }

    /**
     * @return string
     */
    public static function getLatestRemoteHash()
    {
        $process = new Process('git rev-parse --verify origin/master');
        $process->run();

        return $process->getOutput();
    }

    /**
     * @param $processId
     */
    protected function getVolumeIdForProcessId($processId)
    {
        $process = new Process('docker ps -a --no-trunc --format {{.ID}}' . Docker::DELIMITER . '{{.Mounts}}');
        $process->run();

        $dockerProcesses = $process->getOutput();

        $lines = explode("\n", $dockerProcesses);

        foreach ($lines as $line) {
            if (empty($line)) {
                continue;
            }

            $params = explode(Docker::DELIMITER, $line);

            if ($params[0] === $processId) {
                return $params[1];
            }
        }

        return null;
    }
}
