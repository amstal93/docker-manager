<?php

namespace DockerBundle\Services;

class ResponseTable
{
    /**
     * @param array $tableData
     *
     * @return string
     */
    public static function drawStats(array $tableData)
    {
        $html = '<table class="table table-bordered table-condensed table-hover">';
        $run = true;
        foreach ($tableData as $m) {
            if ($run === true) {
                $html .= '<tr>';
                foreach (array_keys($m) as $column) {
                    $html .= '<th>' . str_replace(['{{.', '}}'], '', $column) . '</th>';
                }
                $html .= '</tr>';
                $run = false;
            }

            $html .= '<tr>';
            foreach ($m as $v) {
                $html .= '<td>' . $v . '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</table>';

        return $html;
    }

    /**
     * @param array $tableData
     *
     * @return string
     */
    public static function drawVolumes(array $tableData)
    {
        $html = '<table class="table table-bordered table-condensed table-hover">';
        $html .= '<tr>';
        $html .= '<th width="30" align="center" style="text-align: center;">#</th>';
        $html .= '<th>Driver</th>';
        $html .= '<th>Volume ID</th>';
        $html .= '</tr>';
        foreach ($tableData as $m) {
            if ($m[0] === 'DRIVER') {
                continue;
            }

            $html .= '<tr>';
            $html .= '<td align="center"><a href="#" class="delete-volume" data-volume-id="' . $m[1] . '"><i class="fa fa-trash text-danger"></i></a></td>';
            $html .= '<td>' . $m[0] . '</td>';
            $html .= '<td>' . $m[1] . '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';

        return $html;
    }

    /**
     * @param string $imageId
     *
     * @return string
     */
    public static function createRowId($imageId)
    {
        return str_replace(':', '-', $imageId);
    }

    /**
     * @param array $tableData
     *
     * @return string
     */
    public static function drawImage(array $tableData)
    {
        $html = '<table class="table table-bordered table-condensed table-hover">';
        $run = true;
        foreach ($tableData as $m) {
            if ($run === true) {
                $html .= '<tr>';
                $html .= '<th width="40"><a href="#" id="delete-selected-images" title="delete selected"><i class="fa fa-trash text-danger" title="delete selected"></i></a></th>';
                foreach (array_keys($m) as $column) {
                    $html .= '<th>' . str_replace(['{{.', '}}'], '', $column) . '</th>';
                }
                $html .= '</tr>';
                $run = false;
            }

            $html .= '<tr class="row-' . self::createRowId($m['{{.ID}}']) . '">';
            $html .= '<td><a href="#" class="delete-image" data-image-id="' . $m['{{.ID}}'] . '"><i class="fa fa-trash text-danger"></i></a> <input class="select-images" data-image-id="' . $m['{{.ID}}'] . '" type="checkbox" name="select-images" value="' . $m['{{.ID}}'] . '" /></td>';
            foreach ($m as $v) {
                $html .= '<td>' . $v . '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</table>';

        return $html;
    }

    /**
     * @param array $tableData
     *
     * @return string
     */
    public static function drawNetworks(array $tableData)
    {
        $html = '<table class="table table-bordered table-condensed table-hover">';
        $run = true;

        foreach ($tableData as $m) {
            if ($run === true) {
                $html .= '<tr>';
                $html .= '<th width="30">&nbsp;</th>';
                foreach (array_keys($m) as $column) {
                    $html .= '<th>' . str_replace(['{{.', '}}'], '', $column) . '</th>';
                }
                $html .= '</tr>';
                $run = false;
            }

            $html .= '<tr>';
            $html .= '<td><a href="#" class="delete-network" data-network-id="' . $m['{{.ID}}'] . '"><i class="fa fa-trash text-danger"></i></a></td>';
            foreach ($m as $v) {
                $html .= '<td>' . $v . '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</table>';

        return $html;
    }

    /**
     * @param array $tableData
     *
     * @return string
     */
    public static function simpleTable($tableData)
    {
        $html = '<table class="table table-bordered table-condensed table-hover">';
        foreach ($tableData as $key => $value) {
            if (is_array($value)) {
                $html .= '<tr>';
                foreach ($value as $item) {
                    $html .= '<td>' . $item . '</td>';
                }
                $html .= '</tr>';

                continue;
            }
            $html .= '<tr>';
            $html .= '<td>' . $key . '</td>';
            $html .= '<td>' . $value . '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';

        return $html;
    }

    /**
     * @param array $tableData
     *
     * @return string
     */
    public static function elasticsearchStatus(array $tableData)
    {
        $html = '<table class="table table-bordered table-condensed table-hover">';

        $html .= '<tr><td>Status</td><td>' . $tableData['status'] . '</td></tr>';
        $html .= '<tr><td>Name</td><td>' . $tableData['name'] . '</td></tr>';
        $html .= '<tr><td>Cluster Name</td><td>' . $tableData['cluster_name'] . '</td></tr>';
        $html .= '<tr><td>Version</td><td>' . $tableData['version']['number'] . '</td></tr>';
        $html .= '<tr><td>Lucene Version</td><td>' . $tableData['version']['lucene_version'] . '</td></tr>';

        $html .= '</table>';

        return $html;
    }

    /**
     * @param array $tableData
     *
     * @return string
     */
    public static function drawProcesses(array $tableData)
    {
        $html = '<table class="table table-bordered table-condensed table-hover">';
        $run = true;
        foreach ($tableData as $m) {
            if ($run === true) {
                $html .= '<tr>';
                $html .= '<th width="40"><a href="#" id="delete-selected-processes" title="delete selected"><i class="fa fa-trash text-danger" title="delete selected"></i></a></th>';
                foreach (array_keys($m) as $column) {
                    $html .= '<th>' . str_replace(['{{.', '}}'], '', $column) . '</th>';
                }
                $html .= '</tr>';
                $run = false;
            }

            $rowClass = self::getRowClass($m['{{.Status}}']);

            $html .= '<tr class="row-' . $m['{{.ID}}'] . ' ' . $rowClass . '">';
            $html .= '<td><a href="#" class="delete-process" data-process-id="' . $m['{{.ID}}'] . '"><i class="fa fa-trash text-danger"></i></a> <input class="select-process" data-process-id="' . $m['{{.ID}}'] . '" type="checkbox" name="select-process" value="' . $m['{{.ID}}'] . '" /></td>';
            foreach ($m as $v) {
                $html .= '<td>' . $v . '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</table>';

        return $html;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    protected static function getRowClass($value)
    {
        if (strpos($value, 'Exited') !== false) {
            return 'danger';
        }
        if (strpos($value, 'Up') !== false) {
            return 'success';
        }
        if (strpos($value, 'Restart') !== false) {
            return 'warning';
        }

        return '';
    }
}
