<?php

namespace DockerBundle\Services;

use GuzzleHttp\Client;
use Symfony\Component\Yaml\Yaml;

class DockerFactory
{
    /**
     * @return \DockerBundle\Services\Docker
     */
    public static function createDocker()
    {
        return new Docker(self::createRunner(), self::createParser());
    }

    /**
     * @return \DockerBundle\Services\ResponseParser
     */
    public static function createParser()
    {
        return new ResponseParser();
    }

    /**
     * @return \DockerBundle\Services\Runner
     */
    public static function createRunner()
    {
        return new Runner(self::createGuzzleComander());
    }

    /**
     * @return \DockerBundle\Services\GuzzleCommander
     */
    public static function createGuzzleComander()
    {
        $client = new Client();

        $parameters = [
            'es_host' => self::getParam('APP_ES_HOST', 'localhost'),
            'es_port' => self::getParam('APP_ES_PORT', '9200'),
        ];

        return new GuzzleCommander($client, $parameters);
    }

    /**
     * @param string $name
     * @param null $default
     */
    protected static function getParam($name, $default = null)
    {
        if (array_key_exists($name, $_SERVER)) {
            return $_SERVER[$name];
        }

        return $default;
    }

    /**
     * @return array
     *
     * @throws \Symfony\Component\Yaml\Exception\ParseException
     */
    protected static function getConfig()
    {
        return Yaml::parse(file_get_contents(APP_ROOT . '/config/config.yml'));
    }
}
