<?php

namespace DockerBundle\Services;

class Formatter
{
    /**
     * @return array
     */
    public static function getImagesFormat()
    {
        return [
            '{{.ID}}',
            '{{.Repository}}',
            '{{.Tag}}',
            '{{.Size}}',
        ];
    }

    /**
     * @return array
     */
    public static function getProcessesFormat()
    {
        return [
            '{{.ID}}',
            '{{.Image}}',
            '{{.Command}}',
            '{{.Ports}}',
            '{{.Status}}',
            '{{.Size}}',
            '{{.Names}}',
            '{{.Mounts}}',
            '{{.Networks}}',
        ];
    }

    /**
     * @return array
     */
    public static function getStatsFormat()
    {
        return [
            '{{.Container}}',
            '{{.Name}}',
            '{{.CPUPerc}}',
            '{{.MemUsage}}',
            '{{.NetIO}}',
            '{{.BlockIO}}',
            '{{.MemPerc}}',
            '{{.PIDs}}',
        ];
    }

    /**
     * @return array
     */
    public static function getNetworksFormat()
    {
        return [
            '{{.ID}}',
            '{{.Name}}',
            '{{.Driver}}',
//            '{{.Scope}}',
//            '{{.IPv6}}',
//            '{{.Internal}}',
            '{{.Labels}}',
            '{{.CreatedAt}}',
        ];
    }
}
