<?php

namespace DockerBundle\Services\Twig;

use DockerBundle\Services\ResponseTable;
use Kisphp\Twig\AbstractTwigFunction;
use Kisphp\Twig\IsSafeHtml;

class DrawSimpleTableFunction extends AbstractTwigFunction
{
    use IsSafeHtml;

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'drawSimpleTable';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function ($tableData) {
            if (\is_array($tableData) === false) {
                return $tableData;
            }

            return $this->drawTable($tableData);
        };
    }

    /**
     * @param array $tableData
     *
     * @return string
     */
    protected function drawTable($tableData)
    {
        return ResponseTable::simpleTable($tableData);
    }
}
