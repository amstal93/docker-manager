<?php

namespace DockerBundle\Services\Twig;

use DockerBundle\Services\ResponseTable;
use Kisphp\Twig\AbstractTwigFunction;
use Kisphp\Twig\IsSafeHtml;

class DrawTableImageFunction extends AbstractTwigFunction
{
    use IsSafeHtml;

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'drawTableImage';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function (array $tableData) {
            return $this->drawTable($tableData);
        };
    }

    /**
     * @param array $tableData
     *
     * @return string
     */
    protected function drawTable(array $tableData)
    {
        return ResponseTable::drawImage($tableData);
    }
}
