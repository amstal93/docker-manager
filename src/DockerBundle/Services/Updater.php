<?php

namespace DockerBundle\Services;

use Symfony\Component\Process\Process;

class Updater
{
    /**
     * @var string
     */
    protected $rootDirectory;

    /**
     * @var string
     */
    protected $environmnet;

    /**
     * @param string $rootDirectory
     * @param string $environmnet
     */
    public function __construct(string $rootDirectory, string $environmnet)
    {
        $this->rootDirectory = $rootDirectory;
        $this->environmnet = $environmnet;
    }

    /**
     * @return bool
     */
    public function isUpgradeNeeded()
    {
        $needUpgrade = false;
        $lockFile = $this->getUpgradeLockFile();

        if (is_file($lockFile) === false) {
            touch($lockFile);
        }

        if (filemtime($lockFile) < (time() - $this->getUpdateInterval())) {
            $localGitHash = Runner::getLatestLocalHash();

            $remoteGitHash = Runner::getLatestRemoteHash();

            $needUpgrade = (strcmp($localGitHash, $remoteGitHash) !== 0);

            if (!$needUpgrade) {
                $this->retouchUpgradeLockFile();
            }
        }

        return $needUpgrade;
    }

    /**
     * @return string
     */
    public function getUpgradeLockFile()
    {
        return $this->rootDirectory . '/../var/cache/' . $this->environmnet . '/upgrade.lock';
    }

    public function retouchUpgradeLockFile()
    {
        $lockFile = $this->getUpgradeLockFile();

        unlink($lockFile);
        touch($lockFile);
    }

    /**
     * @return int
     */
    public function getUpdateInterval()
    {
        return 60 * 60 * 24;
    }

    /**
     * @return string
     */
    public function runGit()
    {
        $command = 'cd ../ && git pull';

        return $this->executeProcess($command);
    }

    /**
     * @return string
     */
    public function runComposer()
    {
        $command = 'cd ../ && composer install';

        return $this->executeProcess($command);
    }

    /**
     * @return string
     */
    public function runGulp()
    {
        $command = 'cd ../ && npm run gulp';

        return $this->executeProcess($command);
    }

    /**
     * @param string $command
     *
     * @return string
     */
    protected function executeProcess($command)
    {
        $process = new Process($command);

        $process->start();

        $output = '';
        $process->wait(function ($type, $buffer) use (&$output) {
            $output .= $buffer;
        });

        return $output;
    }
}
