<?php

namespace DockerBundle\Services;

use GuzzleHttp\Client;

class GuzzleCommander
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @var array
     */
    private $parameters = [];

    /**
     * @param \GuzzleHttp\Client $client
     * @param array $parameters
     */
    public function __construct(Client $client, array $parameters)
    {
        $this->client = $client;
        $this->parameters = $parameters;
    }

    /**
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getClusterHealth()
    {
        return $this->executeCommand($this->createEsUrl() . '/_cluster/health?pretty=true');
    }

    /**
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getIndices()
    {
        return $this->executeCommand($this->createEsUrl() . '/_cat/indices?v');
    }

    /**
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getInfo()
    {
        return $this->executeCommand($this->createEsUrl() . '/');
    }

    /**
     * @return string
     */
    protected function createEsUrl()
    {
        return sprintf('http://%s:%d', $this->parameters['es_host'], $this->parameters['es_port']);
    }

    /**
     * @param string $url
     *
     * @return \Psr\Http\Message\StreamInterface|string
     */
    protected function executeCommand($url)
    {
        try {
            $res = $this->client->request('GET', $url);

            return $res->getBody();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
