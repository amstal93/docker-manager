#!/usr/bin/env bash

ENVIRONMENT='prod'

RUN_ALL=1
RUN_F=0
RUN_B=0
RUN_T=0

source _setup/print.sh
source _setup/functions.sh

ARGUMENTS="$@"

# run frontend
if [[ "${ARGUMENTS}" == *"-f"* ]];then
    RUN_ALL=0
    RUN_F=1
fi
# run backend
if [[ "${ARGUMENTS}" == *"-b"* ]];then
    RUN_ALL=0
    RUN_B=1
fi
# run tests
if [[ "${ARGUMENTS}" == *"-t"* ]];then
    RUN_ALL=0
    RUN_T=1
fi
# is dev
if [[ "${ARGUMENTS}" == *"dev"* ]];then
    infoText "Use DEV environment"
    ENVIRONMENT='dev'
fi

if [[ $RUN_ALL == 1 ]];then
    RUN_F=1
    RUN_B=1
    RUN_T=1
fi

before_public

if [[ $ENVIRONMENT == "prod" ]];then
    public_prod
else
    public_dev
fi

post_public
frontend_build

successText "Setup successful"
