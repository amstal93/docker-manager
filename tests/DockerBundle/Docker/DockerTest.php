<?php

namespace tests\Docker;

use DockerBundle\Services\Docker;
use DockerBundle\Services\ResponseParser;
use DockerBundle\Services\Runner;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class DockerTest extends TestCase
{
    /**
     * @var Docker
     */
    protected $docker;

    protected function setUp()
    {
        parent::setUp();

        /** @var Runner $runner */
        $runner = $this->prophesize(Runner::class);
        $runner->removeProcess(Argument::any())->willReturn('anything');
        $runner->removeDockerImage(Argument::any())->willReturn('anything');
        $runner->getImages()->willReturn($this->getImagesResponse());
        $runner->getProcesses()->willReturn($this->getProcessesResponse());
        $runner->getStats()->willReturn($this->getStatsResponse());

        $this->docker = new Docker($runner->reveal(), new ResponseParser());
    }

    public function test_remove_process()
    {
        self::assertNotEmpty($this->docker->removeProcess('1'));
    }

    public function test_remove_image()
    {
        self::assertNotEmpty($this->docker->removeDockerImage('1'));
    }

    public function test_get_images()
    {
        $response = $this->docker->getImages();

        self::assertArrayHasKey('{{.ID}}', $response[1]);
    }

    public function test_get_processes()
    {
        $response = $this->docker->getProcesses();

        self::assertArrayHasKey('{{.ID}}', $response[1]);
    }

    public function test_get_stats()
    {
        $response = $this->docker->getStats();

        self::assertArrayHasKey('{{.Container}}', $response[1]);
    }

    /**
     * @return string
     */
    protected function getImagesResponse()
    {
        $data = [
            [
                '{{.ID}}','{{.Repository}}','{{.Tag}}','{{.Size}}',
            ],
            [
                'id_1','repo_1','tag_1','size_1'
            ],
            [
                'id_2','repo_2','tag_2','size_2'
            ]
        ];

        return $this->buildResponse($data);
    }

    /**
     * @return string
     */
    protected function getProcessesResponse()
    {
        $data = [
            [
                '{{.ID}}','{{.Image}}','{{.Command}}','{{.Ports}}','{{.Status}}','{{.Size}}','{{.Names}}','{{.Mounts}}','{{.Networks}}',
            ],
            [
                'id_1','image_1','command_1','ports_1','status_1','size_1','names_1','mounts_1','networks_1'
            ],
            [
                'id_2','image_2','command_2','ports_2','status_2','size_2','names_2','mounts_2','networks_2'
            ]
        ];

        return $this->buildResponse($data);
    }

    /**
     * @return string
     */
    protected function getStatsResponse()
    {
        $data = [
            [
                '{{.Container}}','{{.Name}}','{{.CPUPerc}}','{{.MemUsage}}','{{.NetIO}}','{{.BlockIO}}','{{.MemPerc}}','{{.PIDs}}',
            ],
            [
                'container_1','name_1','cpuperc_1','memusage_1','netio_1','blockio_1','memperc_1','piids_1'
            ]
        ];

        return $this->buildResponse($data);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function buildResponse(array $data)
    {
        $output = [];
        foreach ($data as $row) {
            $output[] = implode(Docker::DELIMITER, $row);
        }

        return implode("\n", $output);
    }
}
